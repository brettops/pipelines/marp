PDFS = $(shell find -name \*.pdf -type f)
INDEX_MDS = $(shell find -name index.md -type f)
INDEX_HTMLS = $(shell find -name index.html -type f)
SVG_PNGS = $(shell find images/ -name \*.png -type f)

.PHONY: all clean

all:

clean:
	rm -f $(PDFS) $(INDEX_HTMLS) $(INDEX_MDS) $(SVG_PNGS)
