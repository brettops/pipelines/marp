# Marp pipeline

[![pipeline status](https://gitlab.com/brettops/pipelines/marp/badges/main/pipeline.svg)](https://gitlab.com/brettops/pipelines/marp/-/commits/main)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

Build slideshows with [Marp](https://marp.app/).

[View the demo slides](https://brettops.gitlab.io/pipelines/marp/).

## Usage

Create a Markdown file in any directory called `slides.md`, and add the
following front matter:

```yaml
---
marp: true
---
```

Include the pipeline:

```yaml
include:
  - project: brettops/pipelines/marp
    file: include.yml

  # optionally publish to GitLab Pages
  - project: brettops/pipelines/pages
    file: include.yml
```

All `<path>/slides.md` files in the repository will be published at
`<project-pages-url>/<path>/`.
