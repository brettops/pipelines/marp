---
marp: true
---

# Example Slideshow

---

# Header 1

## Header 2

### Header 3

#### Header 4

##### Header 5

###### Header 6

---

# Text

Normal text.

_Italic text._

**Bold text.**

`Monospace text.`

~~Strikethrough text.~~

> Block quote.

---

# Code Blocks

```python
import io

# this comment is important
def my_function():
    pass
```
